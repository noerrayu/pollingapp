import React, { Component } from "react";
import Api from "../services/Api";

class App extends React.Component {
  async componentDidMount() {
    const result = await Api.call("post", "auth/login", {
      username: "username",
      password: "password",
    });

    console.log(result);
  }

  render() {
    return <div>Ayo tidur aja!</div>;
  }
}

export default App;

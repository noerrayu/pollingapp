import axios from "axios";

const host = "http://localhost:4000/api";

export const checkToken = (token) => {
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
};

export const call = async (method, path, data) => {
  // method include get/post/delete
  const response = await axios[method](`${host}/${path}`, data);
  return response.data;
};

export default { call, checkToken };

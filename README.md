**Pollling Application**
for assesment test BCA SYNRGY ACADEMY

**Tools**
* Mongodb
* ExpressJS
* ReactJS
* NodeJS


**User Stories**
* As a user I can Log in 
* As a user I can register
* As an authenticated user I can create poll
* As an authenticated user I can vote poll
* As an authenticated user I can delete poll

**Getting Started**
* create .env files in backend file
`PORT = 4000
DATABASE = 'mongodb://localhost:4000/<DATABASE_NAME>'
SECRET = 'yourSECRETkEy'`


[*] finished : backend
[ ] unfinished : frontend


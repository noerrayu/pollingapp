const router = require("express").Router();

const handler = require("../handlers/errorhandler");
const auth = require("../middlewares/authmiddleware");

router.route("/").get(handler.allPolls).post(auth, handler.createPoll);

router.get("/user", auth, handler.usersPolls);

router
  .route("/:id")
  .get(handler.getPoll)
  .post(auth, handler.vote)
  .delete(auth, handler.deletePoll);

module.exports = router;

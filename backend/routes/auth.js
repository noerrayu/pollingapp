const router = require("express").Router();

const handler = require("../handlers/errorhandler");

router.post("/register", handler.register);
router.post("/login", handler.login);

module.exports = router;

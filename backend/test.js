require("dotenv").config();
const mongoose = require("mongoose");

mongoose.set("debug", true);
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DATABASE);

const db = require("./models/dbmodel");

const users = [
  { username: "username", password: "password" },
  { username: "noer", password: "ayu" },
];

const polls = [
  {
    question: "Manakah film terbaik sepanjang masa?",
    options: ["Harry Potter", "Parasite", "Titanic"],
  },
  { question: "Siapakah yang paling cantik?", options: ["Kamu", "Dia"] },
  { question: "Apakah kopi enak?", options: ["Iya", "Tidak"] },
  { question: "Apakah lebih baik tidur?", options: ["Iya", "Tidak"] },
];

const test = async () => {
  try {
    await db.User.remove();
    console.log("DROP ALL USERS");

    await db.Poll.remove();
    console.log("DROP ALL POLLS");

    await Promise.all(
      users.map(async (user) => {
        const data = await db.User.create(user);
        await data.save();
      })
    );
    console.log("CREATED USERS", JSON.stringify(users));

    await Promise.all(
      polls.map(async (poll) => {
        poll.options = poll.options.map((option) => ({ option, votes: 0 }));
        const data = await db.Poll.create(poll);
        const user = await db.User.findOne({ username: "noer" });
        data.user = user;
        user.polls.push(data._id);
        await user.save();
        await data.save();
      })
    );
    console.log("CREATED POLLS", JSON.stringify(polls));
  } catch (err) {
    console.error(err);
  }
};

test();

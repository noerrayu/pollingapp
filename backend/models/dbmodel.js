// import mongoose
const mongoose = require("mongoose");

mongoose.set("debug", true);
// set mongoose promise to handle async javascript
mongoose.Promise = global.Promise;
// connect to the database
mongoose.connect("mongodb://localhost:27017/polling", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

module.exports.User = require("./usermodel");
module.exports.Poll = require("./pollmodel");

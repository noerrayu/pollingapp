const jwt = require("jsonwebtoken");
const db = require("../models/dbmodel");

exports.register = async (req, res, next) => {
  try {
    const user = await db.User.create(req.body);
    const { id, username } = user;

    const token = jwt.sign({ id, username }, process.env.SECRET);

    res.status(201).json({ id, username, token });
  } catch (err) {
    if (err.code === 11000) {
      err.message = "Username has been used!";
    }

    next(err);
  }
};

exports.login = async (req, res, next) => {
  try {
    // check in database
    const user = await db.User.findOne({ username: req.body.username });
    const { id, username } = user;
    const correct = await user.comparePassword(req.body.password);

    if (correct) {
      const token = jwt.sign({ id, username }, process.env.SECRET);

      res.json({
        id,
        username,
        token,
      });
    } else {
      throw new Error();
    }
  } catch (err) {
    err.message = "Invalid username or password";
    next(err);
  }
};

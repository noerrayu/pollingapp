require("dotenv").config();
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

// import the database
const db = require("./models/dbmodel");
const handler = require("./handlers/errorhandler");
const routes = require("./routes");

const app = express();
const port = process.env.PORT;

app.use(cors());
app.use(bodyParser.json());

app.get("/", (req, res) => res.json({ hello: "All!" }));
app.use("/api/auth", routes.auth);
app.use("/api/polls", routes.poll);

app.use(handler.notFound);
app.use(handler.errors);

app.listen(port, console.log(`Server running on port ${port}`));
